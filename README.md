# Puppet-s3fs_fuse

Puppet-s3fs_fuse is a Puppet module that installs and configures [s3fs_fuse](https://github.com/s3fs-fuse/s3fs-fuse), 
a FUSE-based file system backed by Amazon S3.

The installation is from the s3fs_fuse git repository.  s3fs_fuse is built from the 
master branch.

This module has been used with Puppet 3 and 4 and Debian Wheezy and Jessie.


## class s3fs_fuse


### Parameters

`$aws_access_key_id`: AWS access key.
`$aws_secret_key`: AWS secret key.