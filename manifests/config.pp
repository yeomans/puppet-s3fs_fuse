class s3fs_fuse::config($path="/root/.s3fs-fuse", $aws_access_key_id, $aws_secret_key)
    {
    file
        {
        "$path":
        ensure => file,
        owner => 'root',
        group => 'root',
        mode => '0600',
        content => "$aws_access_key_id:$aws_secret_key\n",
        }
    }