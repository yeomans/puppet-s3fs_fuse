class s3fs_fuse::package
    (
    $ensure="present", 
    $src='https://github.com/s3fs-fuse/s3fs-fuse.git',
    $src_dir='/usr/local/src/s3fs-fuse',
    )
    {
    realize Package['automake']
    realize Package['autotools-dev']
    realize Package['g++']
    realize Package['git']
    realize Package['libcurl4-gnutls-dev']
    realize Package['libfuse-dev']
    realize Package['libssl-dev']
    realize Package['libxml2-dev']
    realize Package['make']
    realize Package['pkg-config']
     
    if $ensure == 'present'
        {
        exec
            {
            "s3fs_fuse_clone":
            command => "/usr/bin/git clone $src $src_dir",
            creates => "$src_dir",
            } ->
            
        exec
            {
            "s3fs_fuse_install":
            cwd => "$src_dir",
            command => "$src_dir/autogen.sh && $src_dir/configure && /usr/bin/make && /usr/bin/make install",
            creates => "/usr/local/bin/s3fs",
            }
        }
    }